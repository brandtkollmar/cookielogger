package de.esag.reactiveexample;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@ComponentScan(basePackages = "de.esag.reactiveexample")
@EnableReactiveMongoRepositories
public class ReactiveExampleConfiguration {
    
    
}
