package de.esag.reactiveexample.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class Author extends DomainObject {
    private String name;
    private Life life;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Author author = (Author) o;
        return Objects.equals(name, author.name) &&
                Objects.equals(life, author.life);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), name, life);
    }

    public Life getLife() {
        return life;
    }

    public void setLife(Life life) {
        this.life = life;
    }

    public String getName() {
    
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", life=" + life +
                "} " + super.toString();
    }
    
    public static Author of(String name, Life life) {
        Author author = new Author();

        author.name = name;
        author.life = life;
        
        return author;
    }
}
