package de.esag.reactiveexample.domain;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.Year;

@Repository
public interface BookRepository extends ReactiveMongoRepository<Book, String> {
    Flux<Book> findAllByAuthorOrderByReleaseDesc(Author author);
    Flux<Book> findAllByRelease(Year release);
}
