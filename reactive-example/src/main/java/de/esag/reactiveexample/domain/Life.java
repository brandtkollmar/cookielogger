package de.esag.reactiveexample.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Year;
import java.util.Objects;

@Document
public class Life {
    private Integer birth;
    private Integer death;

    public Integer getBirth() {
        return birth;
    }

    public Integer getDeath() {
        return death;
    }

    public void setBirth(Integer birth) {
        this.birth = birth;
    }

    public void setDeath(Integer death) {
        this.death = death;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Life life = (Life) o;
        return Objects.equals(birth, life.birth) &&
                Objects.equals(death, life.death);
    }

    @Override
    public int hashCode() {

        return Objects.hash(birth, death);
    }

    @Override
    public String toString() {
        return "Life{" +
                "birth=" + birth +
                ", death=" + death +
                '}';
    }
    
    public static Life of(Year birth) {
        Life life = new Life();
        
        life.birth = birth.getValue();
        
        return life;
    }



    public static Life of(Year birth, Year death) {
        Life life = new Life();

        life.birth = birth.getValue();
        life.death = death.getValue();

        return life;
    }
}
