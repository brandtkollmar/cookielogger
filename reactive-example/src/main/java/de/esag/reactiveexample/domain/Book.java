package de.esag.reactiveexample.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Year;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Document
public class Book extends DomainObject {
    private String title;
    private Integer release;
    @DBRef private Author author;
    private List<String> tags = Collections.emptyList();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Book book = (Book) o;
        return Objects.equals(title, book.title) &&
                Objects.equals(release, book.release) &&
                Objects.equals(author, book.author) &&
                Objects.equals(tags, book.tags);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), title, release, author, tags);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRelease() {
        return release;
    }

    public void setRelease(Integer release) {
        this.release = release;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", release=" + release +
                ", author=" + author +
                ", tags=" + tags +
                "} " + super.toString();
    }

    public static Book of(String title, Year release, Author author, List<String> tags) {

        Book book = new Book();

        book.title = title;
        book.release = release.getValue();
        book.author = author;
        book.tags = tags;

        return book;
    }
}
