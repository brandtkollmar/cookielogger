package de.esag.reactiveexample.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;
import java.util.Objects;

public abstract class DomainObject {
    @Id
    private String objectId;
    
    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainObject that = (DomainObject) o;
        return Objects.equals(objectId, that.objectId) &&
                Objects.equals(lastModifiedDate, that.lastModifiedDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(objectId, lastModifiedDate);
    }

    @Override
    public String toString() {
        return "DomainObject{" +
                "objectId='" + objectId + '\'' +
                ", lastModifiedDate=" + lastModifiedDate +
                '}';
    }
}
