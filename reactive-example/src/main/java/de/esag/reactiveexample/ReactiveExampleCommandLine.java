package de.esag.reactiveexample;

import de.esag.reactiveexample.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.time.Year;
import java.util.Arrays;
import java.util.stream.Collectors;

@SpringBootApplication
@Import(ReactiveExampleConfiguration.class)
public class ReactiveExampleCommandLine implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(ReactiveExampleCommandLine.class);
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    @Autowired
    public ReactiveExampleCommandLine(
            BookRepository bookRepository,
            AuthorRepository authorRepository
    ) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(ReactiveExampleCommandLine.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        authorRepository
                .save(Author.of("George Orwell", Life.of(Year.of(1903), Year.of(1950))))
                .subscribe(author -> LOG.info("Saved " + author));

        authorRepository
                .save(Author.of("Haruki Murakami", Life.of(Year.of(1949))))
                .doOnError(author -> LOG.info("Saving " + author + " failed."))
                .subscribe(author -> LOG.info("Saved " + author));


        authorRepository
                .save(Author.of("Stephen King", Life.of(Year.of(1947))))
                .flatMap(king -> bookRepository.save(
                        Book.of(
                                "The Gunslinger",
                                Year.of(1982),
                                king,
                                Arrays.asList("The Dark Tower", "Fantasy", "western")
                        )
                ))
                .subscribe(roland -> LOG.info("Saved " + roland));


        authorRepository
                .save(Author.of("Stephen King", Life.of(Year.of(1947))))
                .map(king -> Book.of(
                        "The Gunslinger",
                        Year.of(1982),
                        king,
                        Arrays.asList("The Dark Tower", "Fantasy", "western")
                ))
                .flatMap(bookRepository::save)
                .subscribe(roland -> LOG.info("Saved " + roland));

        authorRepository
                .findAll()
                .collect(Collectors.toList())
                .subscribe(allAuthors -> LOG.info(allAuthors.size() + " authors in total"));

        authorRepository
                .findByNameIgnoreCase("stephen king")
                .doOnError(exception -> LOG.error("", exception))
                .subscribe(king -> LOG.info("It's your lucky day! We've got books from " + king.getName()));


        authorRepository
                .findByNameIgnoreCase("Rihito Takarai")
                .defaultIfEmpty(Author.of("Rihito Takarai", Life.of(Year.of(1989))))
                .map(Author::getLife)
                .map(Life::getBirth)
                .subscribe(takaraiSansBirth -> LOG.info("Takarai-san was born in " + takaraiSansBirth));
    }


}
