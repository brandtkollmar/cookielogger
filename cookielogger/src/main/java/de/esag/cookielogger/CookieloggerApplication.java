package de.esag.cookielogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CookieloggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CookieloggerApplication.class, args);
    }


}
