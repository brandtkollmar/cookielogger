package de.esag.cookielogger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;
import java.util.Objects;

/**
 * @author becker
 */
@RestController
public class CookieWeiterleitungEndpoint {
    private static final Logger LOG = LoggerFactory.getLogger(CookieWeiterleitungEndpoint.class);
    
    
    /*private static final String GENO_USER = "GenoUser=YG8BG5E|YG8560|851367838b32f00d154a0b8419e48c83; Domain=sys1.tb.in.gad.de; Path=/; Secure; HttpOnly; secure";
    private static final String GAD_CSRF = "gadCSRF=EECAHse8iKqgBVRfZBTMQg; Domain=sys1.tb.in.gad.de; Path=/; HttpOnly; secure";
    private static final String TOMEPUT_GLOBAL_INITIAL = "TIMEOUT_GLOBAL=1539774019|1539817218; Domain=.sys1.tb.in.gad.de; Path=/\n";
    private static final String NSC_TMAA = "NSC_TMAA=0e2477372a1b93d3c6b047f342a72558; path=/; domain=.i8560.sys1.tb.in.gad.de; HttpOnly; Expires=Tue, 19 Jan 2038 03:14:07 GMT;";
    private static final String NSC_TMAS = "NSC_TMAS=c5b5057e1bb740a32fa0028397f07027; path=/; domain=.i8560.sys1.tb.in.gad.de; Secure; HttpOnly; Expires=Tue, 19 Jan 2038 03:14:07 GMT;";*/


    private static final String GENO_USER = "GenoUser=YG8BG5E|YG8560|851367838b32f00d154a0b8419e48c83; Domain=.localhost; Path=/;";// Secure; HttpOnly; secure";
    private static final String GAD_CSRF = "gadCSRF=EECAHse8iKqgBVRfZBTMQg; Domain=.localhost; Path=/;";// HttpOnly; secure";
    private static final String TIMEOUT_GLOBAL_INITIAL = "TIMEOUT_GLOBAL=1539774019|1539817218; Domain=.localhost; Path=/";
    private static final String TIMEOUT_GLOBAL_MODIFIED = "TIMEOUT_GLOBAL=1539774019|1539819000; Domain=.localhost; Path=/";
    private static final String NSC_TMAA = "NSC_TMAA=0e2477372a1b93d3c6b047f342a72558; path=/; domain=.i8560.sys1.tb.in.gad.de; HttpOnly; Expires=Tue, 19 Jan 2038 03:14:07 GMT";
    private static final String NSC_TMAS = "NSC_TMAS=c5b5057e1bb740a32fa0028397f07027; path=/; domain=.i8560.sys1.tb.in.gad.de; Secure; HttpOnly; Expires=Tue, 19 Jan 2038 03:14:07 GMT";

    @PostMapping(value = "/login", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> login(
            @RequestParam(value = "login", required = false) final String login,
            @RequestParam(value = "passwd", required = false) final String passwd
    ) throws URISyntaxException {

        LOG.info("LOGIN: " + login + " " + passwd);

        return ResponseEntity
                .status(HttpStatus.MOVED_TEMPORARILY)
                .location(new URI("http://localhost:8080/moved"))
                .header("Set-Cookie", NSC_TMAA)
                .header("Set-Cookie", NSC_TMAS)
                .contentType(MediaType.TEXT_PLAIN)
                .body("");
    }

    //@PostMapping(value = "/moved", produces = MediaType.TEXT_PLAIN_VALUE)
    @GetMapping(value = "/moved", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> moved(
            @RequestParam(value = "login", required = false) final String login,
            @RequestParam(value = "passwd", required = false) final String passwd,
            @PathParam(value = "login") final String pathLogin,
            @PathParam(value = "passwd") final String pathPasswd
    ) throws URISyntaxException {

        LOG.info("MOVED: " + login + " " + passwd);
        LOG.info("MOVED: " + pathLogin + " " + pathPasswd);

        return ResponseEntity
                .status(HttpStatus.OK)
                .header("Set-Cookie", GENO_USER)
                .header("Set-Cookie", GAD_CSRF)
                .header("Set-Cookie", TIMEOUT_GLOBAL_INITIAL)
                .contentType(MediaType.TEXT_PLAIN)
                .body("Anwendungsstarter");
    }
}
