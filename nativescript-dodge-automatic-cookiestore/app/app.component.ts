import { Component } from "@angular/core";

import { HttpClient } from "@angular/common/http";

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent {

    content: string = "No request sent";
    cookie: string = "";

    constructor(private readonly httpClient: HttpClient) {
    }

    request() {
        this.httpClient.
            post("http://vido:8080/login?login=something&passwd=secret", "", {
                responseType: "text",
                observe: "response",
                headers: {
                    "Cookie": ""
                },
                withCredentials: true
            })
            .subscribe(
                response => {
                    this.cookie = JSON.stringify(response.headers.getAll("Set-Cookie"))
                    this.content = response.body
                }
            )

    }
}