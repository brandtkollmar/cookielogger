# Samples for various stuff

## reactive-example
> Reactive programming using Spring (Project Reactor) and MongoDB

## cookie-logger
> Spring project for testing cookie behaviour

## nativescript-dodge-automatic-cookiestore
> NativeScript project which makes an HttpRequest while dodging the automatic cookie store (works with cookie-logger)